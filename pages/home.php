<?php
	$slider .= '
	<div class="slider-wrapper">
		<div class="slider">
			<ul class="items">
				<li><img src="images/slider-img1.jpg" alt="" /></li>
				<li><img src="images/slider-img2.jpg" alt="" /></li>
				<li><img src="images/slider-img3.jpg" alt="" /></li>
			</ul>
		</div>
		<a class="prev" href="#">prev</a> <a class="next" href="#">next</a> </div>
	</div>';
	
	$wrapper .= '
	<div class="wrapper p5">
		<article class="grid_4">
			<div class="wrapper">
				<figure class="img-indent"><img src="images/page1-img1.png" alt=""></figure>
				<div class="extra-wrap">
					<h4>Silniki</h4>
					<p class="p2">Nasz zakład zapewnia szeroką paletę usług w zakresie naprawy silników. Od drobnych napraw po całkowitą wymianę silników, oferujemy doskonały serwis, ku pełnemu zadowoleniu naszych klientów.</p>
				</div>
			</div>
		</article>
		<article class="grid_4">
			<div class="wrapper">
				<figure class="img-indent"><img src="images/page1-img2.png" alt=""></figure>
				<div class="extra-wrap">
					<h4>Wulkanizacja i wyważanie opon</h4>
					<p class="p2">Wieloletnie doświadczenie w branży oponiarskiej oraz fakt, iż pracujemy na najnowocześniejszym sprzęcie z najwyższej półki, gwarantuje najwyższą jakość usług, a to, że motoryzacja jest naszą pasją, zapewnia  indywidualne i fachowe podejście do każdego Klienta.
					Zajmujemy się wszystkim procesami związanymi z ogumieniem.</p>
				</div>
			</div>
		</article>
	</div>';

	$content .= '
	<center>
	
	<h2>Zapraszamy do skorzystania z naszych uslug <br><br> Mechanika samochodowa Piotr Gajdek</h2>
	
	</center>';
?>
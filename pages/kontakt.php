<?php
	$content .= '
	<article class="grid_8">
		<h2>Tu nas znajdziesz</h2>
		<figure class="indent-bot">
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d906.8997611703815!2d21.952766804229885!3d49.99073677449377!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x473cf964f0b545d9%3A0x5d9cf1b39fc331ef!2sTeodora+Lubomirskiego+347%2C+36-040+Boguchwa%C5%82a!5e0!3m2!1spl!2spl!4v1530816892395" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
		</figure>
	</article>
	<article class="grid_4">
		<div class="indent-left2 indent-top">
			<div class="box p4">
				<div class="padding">
					<div class="wrapper">
						<figure class="img-indent"><img src="images/page1-img4.png" alt=""></figure>
						<div class="extra-wrap">
							<h3 class="p0">Godziny otwarcia:</h3>
						</div>
					</div>
					<p class="color-1 p0">Poniedziałek - Piątek: 09:00 - 17:00</p>
					<p class="color-1 p1">Soboty: nieczynne</p>
				</div>
			</div>
			<div class="indent-left">
				<dl class="main-address">
					<dt>36-040, Boguchwała<br>ul. Teodora Lubomirskiego 347</dt>
					<dd><span>Telefon:</span>536 288 047</dd>
					<dd><span>E-mail:</span><a href="mailto:piotrgajdek1@gmail.com">piotrgajdek1@gmail.com</a></dd>
				</dl>
			</div>
		</div>
	</article>';
?>
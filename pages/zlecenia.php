<?php
	$zlecenia = $db->query("SELECT * FROM `car_orders` ORDER BY `data_zlozenia_zlecenia` DESC;")->fetchAll();
	$content .= '
	<style>
		table { 
			border-spacing: 10px;
			border-collapse: separate;
			border: 1px solid orange;
		}
	</style>
	<center>
	<table width="98%">
	<tr>
		<td style="font-weight: bold;">Data złożenia zlecenia</td>
		<td style="font-weight: bold;">Marka pojazdu</td>
		<td style="font-weight: bold;">Model pojazdu</td>
		<td style="font-weight: bold;">Generacja pojazdu</td>
		<td style="font-weight: bold;">Imię klienta</td>
		<td style="font-weight: bold;">Telefon kontaktowy</td>
	</tr>';
	foreach($zlecenia as $z)
	{
		$content .= '
		<tr>
			<td>' . date("d.m.Y, H:i:s", $z['data_zlozenia_zlecenia']) . '</td>
			<td>' . $z['car_make_name'] . '</td>
			<td>' . $z['car_model_name'] . '</td>
			<td>' . $z['car_generation_name'] . '</td>
			<td>' . $z['imie'] . '</td>
			<td>' . $z['tel'] . '</td>
		</tr>';
	}
	$content .= '</table></center>';
?>
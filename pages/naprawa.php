<?php
	$slider .= '
	<div class="slider-wrapper">
		<div class="slider">
			<ul class="items">
				<li><img src="images/slider-img1.jpg" alt="" /></li>
				<li><img src="images/slider-img2.jpg" alt="" /></li>
				<li><img src="images/slider-img3.jpg" alt="" /></li>
			</ul>
		</div>
		<a class="prev" href="#">prev</a> <a class="next" href="#">next</a> </div>
	</div>';

	$car_makes = $db->query("SELECT `id_car_make`, `name` FROM `car_make` ORDER BY `name` ASC;")->fetchAll();
	
	$content .= '
	<script>
		$(document).ready(function() {
			$("#marka_pojazdu").select2();
			$("#model_pojazdu").select2();
			$("#generacja_pojazdu").select2();
			
			$("#marka_pojazdu").change(function() {
				if($("#model_pojazdu"))
				{
					$("#model_pojazdu").find("option").remove();
					$("#generacja_pojazdu").find("option").remove();
				}	
				
				var id_marki = $("#marka_pojazdu").val();
				$.ajax({
					type: \'POST\',
					url: \'pages/ajax/modele_pojazdow.php\',
					data: {
						id_marki: id_marki
					},
					success: function(output) {
						$.each(JSON.parse(output), function(idx, obj) {
							$("#model_pojazdu").append(\'<option value="\' + obj.id_car_model + \'">\' + obj.name + \'</option>\');
						});
					},
					error: function(error) {
						
					}
				});
			});
			
			$("#model_pojazdu").change(function() {
				if($("#generacja_pojazdu"))
				{
					$("#generacja_pojazdu").find("option").remove();
				}	
				
				var id_modelu = $("#model_pojazdu option:selected").val();
				$.ajax({
					type: \'POST\',
					url: \'pages/ajax/generacje_pojazdow.php\',
					data: {
						id_modelu: id_modelu
					},
					success: function(output) {
						$.each(JSON.parse(output), function(idx, obj) {
							$("#generacja_pojazdu").append(\'<option value="\' + obj.id_car_generation + \'">\' + obj.name + \' [\' + obj.year_begin + \' - \' + obj.year_end + \']</option>\');
						});
					},
					error: function(error) {
						
					}
				});
			});
			
			$("#potwierdz").click(function() {
				var id_marki = $("#marka_pojazdu option:selected").val();
				var id_modelu = $("#model_pojazdu option:selected").val();
				var id_generacji = $("#generacja_pojazdu option:selected").val();
				var name_marki = $("#marka_pojazdu option:selected").html();
				var name_modelu = $("#model_pojazdu option:selected").html();
				var name_generacji = $("#generacja_pojazdu option:selected").html();
				var imie = $("#imie").val();
				var tel = $("#tel").val();
				
				$.ajax({
					type: \'POST\',
					url: \'pages/ajax/wyslij_zgloszenie.php\',
					data: {
						id_marki: id_marki,
						id_modelu: id_modelu,
						id_generacji: id_generacji,
						name_marki: name_marki,
						name_modelu: name_modelu,
						name_generacji: name_generacji,
						imie: imie,
						tel: tel
					},
					success: function(output) {
						alertify.success("Zgłoszenie zostało wysłane pomyślnie. Prosimy oczekiwać na kontakt od naszego technika.");
					},
					error: function(error) {
						
					}
				});
			});
		});
	</script>
	
	
	<article class="grid_12">
		<center><h2>Wybierz swoj pojazd</h2></center>
		<select id="marka_pojazdu" class="select">';
			foreach($car_makes as $car_make)
			{
				$content .= '<option value="' . $car_make['id_car_make'] . '">' . $car_make['name'] . '</option>';
			}
			$content .= '
		</select><br /><br />
		<select id="model_pojazdu" class="select"></select><br /><br />
		<select id="generacja_pojazdu" class="select"></select><br /><br />
		<input id="imie" type="text" placeholder="Wpisz swoje imię..." style="width: 99%; border-radius: 5px; height: 20px;"/><br /><br />
		<input id="tel" type="text" placeholder="Podaj numer telefonu..." style="width: 99%; border-radius: 5px; height: 20px;"/><br /><br />
		<center><input id="potwierdz" type="button" value="Wyślij zgłoszenie" /></center>
	</article>';
?>
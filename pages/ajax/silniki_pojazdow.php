<?php
	require_once('../../config.php');
	require_once('../../database.php');

	$id_generacji = $_POST['id_generacji'];
	$car_trims = $db->query("SELECT `id_car_trim`, `name` FROM `car_trim` WHERE `id_car_model` = " . (int)$id_generacji . " ORDER BY `id_car_trim` ASC;;")->fetchAll();
	
	$array = array();
	
	foreach($car_trims as $car_trim)
	{
		$array[] = array('id_car_trim' => $car_trim['id_car_trim'], 'name'=> $car_trim['name']); 
	}
	
	echo json_encode($array);
?>
<?php
	require_once('../../config.php');
	require_once('../../database.php');

	$id_marki = $_POST['id_marki'];
	$car_models = $db->query("SELECT `id_car_model`, `name` FROM `car_model` WHERE `id_car_make` = " . (int)$id_marki . ";")->fetchAll();
	
	$array = array();
	
	foreach($car_models as $car_model)
	{
		$array[] = array('id_car_model' => $car_model['id_car_model'], 'name'=> $car_model['name']); 
	}
	
	echo json_encode($array);
?>
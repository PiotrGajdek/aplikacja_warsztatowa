<?php
	require_once('../../config.php');
	require_once('../../database.php');

	$id_marki = (int)$_POST['id_marki'];
	$id_modelu = (int)$_POST['id_modelu'];
	$id_generacji = (int)$_POST['id_generacji'];
	$name_marki = $_POST['name_marki'];
	$name_modelu = $_POST['name_modelu'];
	$name_generacji = $_POST['name_generacji'];
	$imie = $_POST['imie'];
	$tel = $_POST['tel'];
	$data_zlozenia = time();
	
	$db->query("INSERT INTO `car_orders`
	(
		`car_make_id`, 
		`car_make_name`, 
		`car_model_id`, 
		`car_model_name`, 
		`car_generation_id`, 
		`car_generation_name`, 
		`data_zlozenia_zlecenia`, 
		`imie`, 
		`tel`
	)
	VALUES
	(
		" . $id_marki . ", 
		" . $db->quote($name_marki) . ", 
		" . $id_modelu . ", 
		" . $db->quote($name_modelu) . ", 
		" . $id_generacji . ", 
		" . $db->quote($name_generacji) . ", 
		" . $data_zlozenia . ", 
		" . $db->quote($imie) . ", 
		" . $tel . "
	)");
?>
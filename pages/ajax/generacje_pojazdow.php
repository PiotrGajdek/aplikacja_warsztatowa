<?php
	require_once('../../config.php');
	require_once('../../database.php');

	$id_modelu = $_POST['id_modelu'];
	$car_generations = $db->query("SELECT `id_car_generation`, `name`, `year_begin`, `year_end` FROM `car_generation` WHERE `id_car_model` = " . (int)$id_modelu . " ORDER BY `year_begin` ASC;")->fetchAll();
	
	$array = array();
	
	foreach($car_generations as $car_generation)
	{
		$array[] = array('id_car_generation' => $car_generation['id_car_generation'], 'name'=> $car_generation['name'], 'year_begin' => $car_generation['year_begin'], 'year_end'=> $car_generation['year_end']); 
	}
	
	echo json_encode($array);
?>
<?php
	require_once('../../config.php');
	require_once('../../database.php');

	$id_modelu = $_POST['id_modelu'];
	$car_trims = $db->query("SELECT `id_car_trim`, `name` FROM `car_trim` WHERE `id_car_model` = " . (int)$id_modelu . " ORDER BY `id_car_trim` ASC;;")->fetchAll();
	
	$array = array();
	
	foreach($car_trims as $car_trim)
	{
		$array[] = array('id_car_trim' => $car_trim['id_car_trim'], 'name'=> $car_trim['name']); 
	}
	
	echo json_encode($array);
?>
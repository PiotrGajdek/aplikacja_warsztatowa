<?php	
	date_default_timezone_set('Europe/Warsaw');
	ob_start();
	session_save_path('sessions/');
	session_start();
	
	require_once('config.php');
	require_once('database.php');
	
	if(empty($_GET['p']))
	{
		require_once("pages/home.php");
	}
	if(isset($_GET['p']))
	{
		if(file_exists('pages/'.$_GET['p'].'.php') && !preg_match("/\./", $_GET['p']) && !preg_match("/%/", $_GET['p']))
		{
			require_once('pages/' . $_GET['p'] . '.php');
		}
		//else
			//header('Location: http://' . $_SERVER['HTTP_HOST'] . '/' . $subfolder . '/');
	}

	require('layout.php');
	ob_end_flush();
?>
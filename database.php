<?php
	require_once('config.php');

	try {
		$db = new PDO('mysql:host=' . $config['database']['host'] . ';dbname=' . $config['database']['database'], $config['database']['username'], $config['database']['password']);
	} catch (PDOException $e) {
		die('Problem z połączeniem do bazy danych.');
	}
	
	$db->query('SET NAMES utf8');
	$db->query('SET CHARACTER_SET utf8_unicode_ci');
?>